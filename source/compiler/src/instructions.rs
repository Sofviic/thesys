use std::sync::atomic::{AtomicUsize, Ordering};

pub type Value = i64;

#[derive(Debug, Clone)]
pub enum Instruction {
    Noa,
    Add(Argument, Argument, Argument),
    Sub(Argument, Argument, Argument),
    Mul(Argument, Argument, Argument),
    Div(Argument, Argument, Argument),
    Mod(Argument, Argument, Argument),
    Equal(Argument, Argument, Argument),
    Less(Argument, Argument, Argument),
    Greater(Argument, Argument, Argument),
    Lnot(Argument, Argument),
    Bnot(Argument, Argument),
    Xor(Argument, Argument, Argument),
    Or(Argument, Argument, Argument),
    And(Argument, Argument, Argument),
    Shl(Argument, Argument, Argument),
    Shr(Argument, Argument, Argument),
    Move(Argument, Argument),
    Rsp(Argument),
    Push(Argument),
    Pop(Argument),
    Jump(Argument),
    Cjump {
        cond: Argument,
        pc: Argument,
    },
    Rpc(Argument),
    Spawn {
        pc: Argument,
        sp: Argument,
    },
    Halt,
    Accept {
        id: Argument,
    },
    Read {
        id: Argument,
        value: Argument,
        error: Argument,
    },
    Write {
        id: Argument,
        value: Argument,
        error: Argument,
    },

    _Label(Label),
}

#[derive(Debug, Clone)]
pub enum Argument {
    Immediate(Value),
    Pointer(Value),
    Stack(Value),
    StackPointer(Value),

    _LabelImmediate(Label),
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Label(usize);
static LABEL_COUNTER: AtomicUsize = AtomicUsize::new(0);
impl Label {
    pub fn new() -> Self {
        Self(LABEL_COUNTER.fetch_add(1, Ordering::Relaxed))
    }
}

impl Instruction {
    pub fn map(self, mut f: impl FnMut(Argument) -> Argument) -> Self {
        match self {
            Instruction::Noa => Instruction::Noa,
            Instruction::Add(x, y, z) => Instruction::Add(f(x), f(y), f(z)),
            Instruction::Sub(x, y, z) => Instruction::Sub(f(x), f(y), f(z)),
            Instruction::Mul(x, y, z) => Instruction::Mul(f(x), f(y), f(z)),
            Instruction::Div(x, y, z) => Instruction::Div(f(x), f(y), f(z)),
            Instruction::Mod(x, y, z) => Instruction::Mod(f(x), f(y), f(z)),
            Instruction::Equal(x, y, z) => Instruction::Equal(f(x), f(y), f(z)),
            Instruction::Less(x, y, z) => Instruction::Less(f(x), f(y), f(z)),
            Instruction::Greater(x, y, z) => Instruction::Greater(f(x), f(y), f(z)),
            Instruction::Xor(x, y, z) => Instruction::Xor(f(x), f(y), f(z)),
            Instruction::Or(x, y, z) => Instruction::Or(f(x), f(y), f(z)),
            Instruction::And(x, y, z) => Instruction::And(f(x), f(y), f(z)),
            Instruction::Shl(x, y, z) => Instruction::Shl(f(x), f(y), f(z)),
            Instruction::Shr(x, y, z) => Instruction::Shr(f(x), f(y), f(z)),
            Instruction::Lnot(x, y) => Instruction::Lnot(f(x), f(y)),
            Instruction::Bnot(x, y) => Instruction::Bnot(f(x), f(y)),
            Instruction::Move(x, y) => Instruction::Move(f(x), f(y)),
            Instruction::Rsp(x) => Instruction::Rsp(f(x)),
            Instruction::Push(x) => Instruction::Push(f(x)),
            Instruction::Pop(x) => Instruction::Pop(f(x)),
            Instruction::Jump(x) => Instruction::Jump(f(x)),
            Instruction::Rpc(x) => Instruction::Rpc(f(x)),
            Instruction::Cjump { cond, pc } => Instruction::Cjump {
                cond: f(cond),
                pc: f(pc),
            },
            Instruction::Spawn { pc, sp } => Instruction::Spawn {
                pc: f(pc),
                sp: f(sp),
            },
            Instruction::Halt => Instruction::Halt,
            Instruction::Accept { id } => Instruction::Accept { id: f(id) },
            Instruction::Read { id, value, error } => Instruction::Read {
                id: f(id),
                value: f(value),
                error: f(error),
            },
            Instruction::Write { id, value, error } => Instruction::Write {
                id: f(id),
                value: f(value),
                error: f(error),
            },
            x => x,
        }
    }
    pub fn index(&self) -> usize {
        match self {
            Instruction::Noa => 0,
            Instruction::Add(_, _, _) => 1,
            Instruction::Sub(_, _, _) => 2,
            Instruction::Mul(_, _, _) => 3,
            Instruction::Div(_, _, _) => 4,
            Instruction::Mod(_, _, _) => 5,
            Instruction::Equal(_, _, _) => 6,
            Instruction::Less(_, _, _) => 7,
            Instruction::Greater(_, _, _) => 8,
            Instruction::Lnot(_, _) => 9,
            Instruction::Bnot(_, _) => 10,
            Instruction::Xor(_, _, _) => 11,
            Instruction::Or(_, _, _) => 12,
            Instruction::And(_, _, _) => 13,
            Instruction::Shl(_, _, _) => 14,
            Instruction::Shr(_, _, _) => 15,
            Instruction::Move(_, _) => 16,
            Instruction::Rsp(_) => 17,
            Instruction::Push(_) => 18,
            Instruction::Pop(_) => 19,
            Instruction::Jump(_) => 20,
            Instruction::Cjump { .. } => 21,
            Instruction::Rpc(_) => 22,
            Instruction::Spawn { .. } => 23,
            Instruction::Halt => 24,
            Instruction::Accept { .. } => 25,
            Instruction::Read { .. } => 26,
            Instruction::Write { .. } => 27,
            Instruction::_Label(_) => unreachable!(),
        }
    }

    pub fn serialize(&self, out: &mut Vec<Value>, label_uses: &mut Vec<(usize, Label)>) {
        let mut opcode = self.index() as Value;
        let mut i = 8;
        let k = out.len();
        out.push(0);
        self.clone().map(|arg| {
            let arg = match arg {
                Argument::_LabelImmediate(label) => {
                    label_uses.push((out.len(), label));
                    Argument::Immediate(0)
                }
                am => am,
            };
            opcode |= (arg.mode_index() << i) as Value;
            out.push(arg.value());
            i += 2;
            arg
        });
        out[k] = opcode;
    }
}

impl Argument {
    pub fn mode_index(&self) -> usize {
        match self {
            Argument::Immediate(_) => 0,
            Argument::Pointer(_) => 1,
            Argument::Stack(_) => 2,
            Argument::StackPointer(_) => 3,
            Argument::_LabelImmediate(_) => unreachable!(),
        }
    }
    pub fn value(&self) -> Value {
        match self {
            Argument::Immediate(x) => *x,
            Argument::Pointer(x) => *x,
            Argument::Stack(x) => *x,
            Argument::StackPointer(x) => *x,
            Argument::_LabelImmediate(_) => 0,
        }
    }
}
