use crate::{
    ast::{BinaryOp, ExprNode, ExprNodeK, Literal},
    instructions::{Argument, Instruction, Label},
    symbols::{Symbol, SymbolData},
    types::TypeSlot,
};
use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
};

use Argument::*;
use Instruction::*;

pub fn generate_code(ast: ExprNode<TypeSlot, Symbol>) -> Vec<Instruction> {
    fn traverse(
        ast: &ExprNode<TypeSlot, Symbol>,
        stack: &mut Stack,
        syms: &mut HashMap<Symbol, Arc<StackElement>>,
    ) -> (Vec<Instruction>, Argument) {
        let mut insts = Vec::new();
        match &ast.k {
            ExprNodeK::Let { name, value, .. } => {
                let (is, r) = traverse(&value, stack, syms);
                insts.extend(is);
                let var = stack.allocate(&match &name.data {
                    SymbolData::Value { r#type, .. } => r#type.clone(),
                    SymbolData::Type => unreachable!(),
                });
                syms.insert(name.to_owned(), var.clone());
                insts.push(Move(var.argument(), r));
                (insts, Argument::Immediate(0))
            }
            ExprNodeK::Symbol(s) => (insts, syms.get(&s).expect("not defined").argument()),
            ExprNodeK::Block(a) => {
                let mut result = Argument::Immediate(0);
                for a in a {
                    let (is, r) = traverse(a, stack, syms);
                    insts.extend(is);
                    result = r;
                }
                (insts, result)
            }
            ExprNodeK::Invoke {
                arguments,
                function,
            } => {
                let mut args = Vec::new();
                for a in arguments {
                    let (is, r) = traverse(a, stack, syms);
                    insts.extend(is);
                    args.push(r);
                }

                let (fis, fun) = traverse(function, stack, syms);
                insts.extend(fis);

                drop((fun, args));

                (insts, Argument::Immediate(0))
            }
            ExprNodeK::Literal(Literal::Int(n)) => (insts, Immediate(*n)),
            ExprNodeK::BinaryOp { op, lhs, rhs } => {
                let (lhs_is, lhs) = traverse(&lhs, stack, syms);
                let (rhs_is, rhs) = traverse(&rhs, stack, syms);
                insts.extend(lhs_is);
                insts.extend(rhs_is);
                let var = stack.allocate(&ast.data);
                insts.push(match op {
                    BinaryOp::Add => Add(var.argument(), lhs, rhs),
                    BinaryOp::Sub => Sub(var.argument(), lhs, rhs),
                    BinaryOp::Mul => Mul(var.argument(), lhs, rhs),
                    BinaryOp::Div => Div(var.argument(), lhs, rhs),
                    BinaryOp::Mod => Mod(var.argument(), lhs, rhs),
                });
                (insts, var.argument())
            }
            x => todo!("{x:?}"),
        }
    }

    traverse(&ast, &mut Stack::default(), &mut HashMap::new()).0
}

#[derive(Clone, Debug, Default)]
pub struct ValueSlot(pub Arc<RwLock<Option<Value>>>);

#[derive(Debug)]
pub enum Value {
    Function(Label),
    Stack { offset: usize },
}

#[derive(Debug, Default)]
pub struct Stack {
    pub symbols: Vec<Arc<StackElement>>,
    pub size: usize,
}

#[derive(Debug)]
pub struct StackElement {
    pub r#type: TypeSlot,
    pub offset: usize,
    pub size: usize,
}

impl Stack {
    pub fn allocate(&mut self, r#type: &TypeSlot) -> Arc<StackElement> {
        let s = Arc::new(StackElement {
            offset: self.size,
            size: r#type.size(),
            r#type: r#type.clone(),
        });
        self.size += r#type.size();
        self.symbols.push(s.clone());
        s
    }
}
impl StackElement {
    pub fn argument(&self) -> Argument {
        Argument::Stack(self.offset as i64)
    }
}
