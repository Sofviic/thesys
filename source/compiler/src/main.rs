#![feature(result_flattening, iterator_try_collect, lazy_cell)]
use crate::{
    ast::Declaration,
    code::generate_code,
    linemap::{Linemap, SourceStr},
    parser::parse_file,
    symbols::resolve_symbols,
    types::resolve_types,
};
use anyhow::anyhow;
use instructions::{Instruction, Value};
use std::{collections::HashMap, fs::read_to_string};
use symbols::Symbol;
use types::Types;

pub mod as_parser;
pub mod ast;
pub mod code;
pub mod instructions;
pub mod linemap;
pub mod parser;
pub mod symbols;
pub mod types;

fn main() -> anyhow::Result<()> {
    let source = read_to_string(
        std::env::args()
            .nth(1)
            .ok_or(anyhow!("usage: compiler <source>"))?,
    )?;

    let lm = Linemap::new(&source);
    let s = SourceStr::new(&source, &lm);

    let (rest, decls, err) = parse_file()(s)?;
    if let Some(err) = err {
        eprintln!("error {err}")
    }
    eprintln!("left over {rest}");

    let mut globals = decls
        .iter()
        .filter_map(|d| match d {
            Declaration::Function(f) => Some((f.name.to_owned(), Symbol::new_value(&f.name))),
        })
        .collect::<HashMap<_, _>>();

    let types = Types::register(&mut globals);

    for decl in decls {
        println!("{decl}");
        match decl {
            Declaration::Function(func) => {
                let body = resolve_symbols(func.body, &globals)?;
                println!("{body}");
                let body = resolve_types(body, &types)?;
                println!("{body}");
                let insts = generate_code(body);
                println!(
                    "{}",
                    insts
                        .iter()
                        .map(|i| format!("{i:?}"))
                        .collect::<Vec<String>>()
                        .join("\n")
                );
                let out = expand_labels(insts);
                eprintln!("{out:?}");
            }
        }
    }
    Ok(())
}

pub fn expand_labels(insts: Vec<Instruction>) -> Vec<i64> {
    let mut label_values = HashMap::new();
    let mut label_uses = Vec::new();

    let mut out = Vec::new();
    for i in insts {
        match i {
            Instruction::_Label(l) => {
                label_values.insert(l, out.len());
            }
            i => i.serialize(&mut out, &mut label_uses),
        }
    }

    for (target, label) in label_uses {
        let value = *label_values.get(&label).unwrap();
        out[target] = value as Value;
    }
    out
}
