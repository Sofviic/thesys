use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, Clone, PartialEq)]
pub struct ExprNode<Data, Symbol> {
    pub data: Data,
    pub k: ExprNodeK<Data, Symbol>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum ExprNodeK<Data, Symbol> {
    Let {
        name: Symbol,
        explicit_type: Option<Box<TypeA<Symbol>>>,
        value: Box<ExprNode<Data, Symbol>>,
    },
    Symbol(Symbol),
    Block(Vec<ExprNode<Data, Symbol>>),
    Invoke {
        function: Box<ExprNode<Data, Symbol>>,
        arguments: Vec<ExprNode<Data, Symbol>>,
    },
    If {
        condition: Box<ExprNode<Data, Symbol>>,
        cond_true: Box<ExprNode<Data, Symbol>>,
        cond_false: Box<ExprNode<Data, Symbol>>,
    },
    While {
        condition: Box<ExprNode<Data, Symbol>>,
        body: Box<ExprNode<Data, Symbol>>,
    },
    Literal(Literal),
    BinaryOp {
        op: BinaryOp,
        lhs: Box<ExprNode<Data, Symbol>>,
        rhs: Box<ExprNode<Data, Symbol>>,
    },
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum BinaryOp {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
}

#[derive(Debug, Clone, PartialEq)]
pub struct TypeA<Symbol> {
    pub name: Symbol,
    pub args: Vec<TypeA<Symbol>>,
}

pub struct Function<Data, Symbol> {
    pub name: String,
    pub arguments: Vec<(String, TypeA<Symbol>)>,
    pub body: ExprNode<Data, Symbol>,
}

pub enum Declaration<Data, Symbol> {
    Function(Function<Data, Symbol>),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
    Int(i64),
    Float(f64),
    String(String),
}

impl<Data, Symbol: Display> Display for Declaration<Data, Symbol> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Declaration::Function(Function {
                name,
                arguments: _,
                body,
            }) => {
                writeln!(f, "function {name:?}")?;
                body.format_with_indent(f, 1)
            }
        }
    }
}

impl<Data, Symbol: Display> ExprNode<Data, Symbol> {
    fn format_with_indent(&self, f: &mut Formatter<'_>, indent: usize) -> std::fmt::Result {
        fn print_node<Data, Symbol: Display>(
            ident: usize,
            f: &mut std::fmt::Formatter<'_>,
            label: Option<&str>,
            node: &ExprNode<Data, Symbol>,
        ) -> std::fmt::Result {
            for _ in 0..ident * 2 {
                write!(f, " ")?;
            }
            if let Some(label) = label {
                write!(f, "{label}: ")?;
            }
            match &node.k {
                ExprNodeK::Let {
                    name,
                    explicit_type: _,
                    value,
                } => {
                    writeln!(f, "let {name} = ")?;
                    print_node(ident + 1, f, None, &value)?;
                }
                ExprNodeK::Symbol(sym) => {
                    writeln!(f, "symbol {sym}")?;
                }
                ExprNodeK::Block(content) => {
                    writeln!(f, "block")?;
                    for s in content {
                        print_node(ident + 1, f, None, &s)?;
                    }
                }
                ExprNodeK::Invoke {
                    function,
                    arguments,
                } => {
                    writeln!(f, "invoke")?;
                    print_node(ident + 1, f, Some("function"), &function)?;
                    for s in arguments {
                        print_node(ident + 1, f, Some("argument"), &s)?;
                    }
                }
                ExprNodeK::If {
                    condition,
                    cond_true,
                    cond_false,
                } => {
                    writeln!(f, "if")?;
                    print_node(ident + 1, f, Some("condition"), &condition)?;
                    print_node(ident + 1, f, Some("cond_true"), &cond_true)?;
                    print_node(ident + 1, f, Some("cond_false"), &cond_false)?;
                }
                ExprNodeK::While { condition, body } => {
                    writeln!(f, "while")?;
                    print_node(ident + 1, f, Some("condition"), &condition)?;
                    print_node(ident + 1, f, Some("body"), &body)?;
                }
                ExprNodeK::Literal(lit) => {
                    writeln!(f, "literal {lit:?}")?;
                }
                ExprNodeK::BinaryOp { op, lhs, rhs } => {
                    writeln!(f, "bop {op:?}")?;
                    print_node(ident + 1, f, Some("lhs"), &lhs)?;
                    print_node(ident + 1, f, Some("rhs"), &rhs)?;
                }
            }
            Ok(())
        }
        print_node(indent, f, None, self)
    }
}
impl<Data, Symbol: Display> Display for ExprNode<Data, Symbol> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.format_with_indent(f, 0)
    }
}

impl<Data, Symbol> ExprNode<Data, Symbol> {
    pub fn map<DataTo>(self, f: &impl Fn(Data) -> DataTo) -> ExprNode<DataTo, Symbol> {
        ExprNode {
            data: f(self.data),
            k: match self.k {
                ExprNodeK::Let {
                    name,
                    explicit_type,
                    value,
                } => ExprNodeK::Let {
                    name,
                    explicit_type,
                    value: Box::new(value.map(f)),
                },
                ExprNodeK::Symbol(s) => ExprNodeK::Symbol(s),
                ExprNodeK::Block(stmts) => {
                    ExprNodeK::Block(stmts.into_iter().map(|e| e.map(f)).collect())
                }
                ExprNodeK::Invoke {
                    function,
                    arguments,
                } => ExprNodeK::Invoke {
                    function: Box::new(function.map(f)),
                    arguments: arguments.into_iter().map(|e| e.map(f)).collect(),
                },
                ExprNodeK::If {
                    condition,
                    cond_true,
                    cond_false,
                } => ExprNodeK::If {
                    condition: Box::new(condition.map(f)),
                    cond_true: Box::new(cond_true.map(f)),
                    cond_false: Box::new(cond_false.map(f)),
                },
                ExprNodeK::While { condition, body } => ExprNodeK::While {
                    condition: Box::new(condition.map(f)),
                    body: Box::new(body.map(f)),
                },
                ExprNodeK::Literal(lit) => ExprNodeK::Literal(lit),
                ExprNodeK::BinaryOp { op, lhs, rhs } => ExprNodeK::BinaryOp {
                    op,
                    lhs: Box::new(lhs.map(f)),
                    rhs: Box::new(rhs.map(f)),
                },
            },
        }
    }
}

impl<Data, Symbol> ExprNode<Data, Symbol> {
    pub fn traverse_pre<Context>(
        &mut self,
        c: &Context,
        f: &mut impl FnMut(&mut Data, &Context, &ExprNodeK<Data, Symbol>) -> Context,
    ) {
        let cc = f(&mut self.data, &c, &self.k);
        match &mut self.k {
            ExprNodeK::Let { value, .. } => value.traverse_pre(&cc, f),
            ExprNodeK::Block(stmts) => {
                for s in stmts {
                    s.traverse_pre(&cc, f);
                }
            }
            ExprNodeK::Invoke {
                function,
                arguments,
            } => {
                function.traverse_pre(&cc, f);
                for a in arguments {
                    a.traverse_pre(&cc, f);
                }
            }
            ExprNodeK::If {
                condition,
                cond_true,
                cond_false,
            } => {
                condition.traverse_pre(&cc, f);
                cond_true.traverse_pre(&cc, f);
                cond_false.traverse_pre(&cc, f);
            }
            ExprNodeK::While { condition, body } => {
                condition.traverse_pre(&cc, f);
                body.traverse_pre(&cc, f);
            }
            ExprNodeK::BinaryOp { lhs, rhs, .. } => {
                lhs.traverse_pre(&cc, f);
                rhs.traverse_pre(&cc, f);
            }
            ExprNodeK::Symbol(_) | ExprNodeK::Literal(_) => (),
        }
    }
}

impl<S: Display> Display for TypeA<S> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let Self { args, name } = self;
        if args.is_empty() {
            f.write_fmt(format_args!("{name}"))
        } else {
            f.write_fmt(format_args!(
                "{name}<{}>",
                args.iter()
                    .map(|a| format!("{a}"))
                    .collect::<Vec<_>>()
                    .join(", ")
            ))
        }
    }
}

// impl<Data, Symbol> ExprNode<A> {
//     pub fn map<B: AstStage>(
//         self,
//         f_data: impl Fn(&ExprNodeK<A>, A::Data) -> B::Data,
//         f_sym: impl Fn(&ExprNodeK<A>, A::Symbol) -> B::Symbol,
//         f_typesym: impl Fn(&ExprNodeK<A>, A::TypeSymbol) -> B::TypeSymbol,
//     ) -> ExprNode<B> {
//         let data = f_data(&self.k, self.data);
//         match self.k.clone() {
//             ExprNodeK::Let {
//                 name,
//                 explicit_type,
//                 value,
//             } => ExprNode {
//                 data,
//                 k: ExprNodeK::Let {
//                     name: f_sym(&self.k, name),
//                     explicit_type: explicit_type.map(|_| todo!()),
//                     value: Box::new(value.map(f_data, f_sym, f_typesym)),
//                 },
//             },
//             ExprNodeK::Symbol(s) => ExprNode {
//                 data,
//                 k: ExprNodeK::Symbol(f_sym(&self.k, s)),
//             },
//             ExprNodeK::Block(b) => ExprNode {
//                 data,
//                 k: ExprNodeK::Block(
//                     b.into_iter()
//                         .map(|n| n.map(&f_data, &f_sym, &f_typesym))
//                         .collect(),
//                 ),
//             },
//             ExprNodeK::Invoke {
//                 function,
//                 arguments,
//             } => ExprNode {
//                 data,
//                 k: ExprNodeK::Invoke {
//                     function: Box::new(function.map(&f_data, &f_sym, &f_typesym)),
//                     arguments: arguments
//                         .into_iter()
//                         .map(|n| n.map(&f_data, &f_sym, &f_typesym))
//                         .collect(),
//                 },
//             },
//             ExprNodeK::If {
//                 condition,
//                 cond_true,
//                 cond_false,
//             } => ExprNode {
//                 data,
//                 k: ExprNodeK::If {
//                     condition: Box::new(condition.map(&f_data, &f_sym, &f_typesym)),
//                     cond_true: Box::new(cond_true.map(&f_data, &f_sym, &f_typesym)),
//                     cond_false: Box::new(cond_false.map(&f_data, &f_sym, &f_typesym)),
//                 },
//             },
//             ExprNodeK::While { condition, body } => ExprNode {
//                 data,
//                 k: ExprNodeK::While {
//                     condition: Box::new(condition.map(&f_data, &f_sym, &f_typesym)),
//                     body: Box::new(body.map(&f_data, &f_sym, &f_typesym)),
//                 },
//             },
//             ExprNodeK::Literal(x) => ExprNode {
//                 data,
//                 k: ExprNodeK::Literal(x),
//             },
//             ExprNodeK::BinaryOp { op, lhs, rhs } => ExprNode {
//                 data,
//                 k: ExprNodeK::BinaryOp {
//                     op,
//                     lhs: Box::new(lhs.map(&f_data, &f_sym, &f_typesym)),
//                     rhs: Box::new(rhs.map(&f_data, &f_sym, &f_typesym)),
//                 },
//             },
//         }
//     }
// }
