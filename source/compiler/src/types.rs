use crate::{
    ast::{ExprNode, ExprNodeK, Literal, TypeA},
    symbols::{Symbol, SymbolData},
};
use anyhow::Result;
use std::{
    collections::HashMap,
    ops::{Deref, DerefMut},
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, RwLock,
    },
};

static CHANGED: AtomicBool = AtomicBool::new(false);

#[derive(Debug, Default, Clone)]
pub struct TypeSlot(pub Arc<RwLock<Option<TypeA<Symbol>>>>);
pub struct Types {
    unit: TypeA<Symbol>,
    bool: TypeA<Symbol>,
    i32: TypeA<Symbol>,
    f32: TypeA<Symbol>,
}

impl Types {
    pub fn register(globals: &mut HashMap<String, Symbol>) -> Self {
        let unit = Symbol::new_type("unit");
        globals.insert("unit".to_owned(), unit.clone());
        let bool = Symbol::new_type("bool");
        globals.insert("bool".to_owned(), bool.clone());
        let f32 = Symbol::new_type("f32");
        globals.insert("f32".to_owned(), f32.clone());
        let i32 = Symbol::new_type("i32");
        globals.insert("i32".to_owned(), i32.clone());
        Self {
            bool: TypeA {
                name: bool,
                args: vec![],
            },
            f32: TypeA {
                name: f32,
                args: vec![],
            },
            i32: TypeA {
                name: i32,
                args: vec![],
            },
            unit: TypeA {
                name: unit,
                args: vec![],
            },
        }
    }
}

pub fn resolve_types(
    ast: ExprNode<(), Symbol>,
    types: &Types,
) -> Result<ExprNode<TypeSlot, Symbol>> {
    let mut ast = ast.map(&|()| TypeSlot::default());
    let mut unchanged = false;
    loop {
        ast.traverse_pre(&(), &mut |data, _pc, node| match node {
            ExprNodeK::Let {
                name,
                explicit_type,
                value,
            } => {
                if let Some(t) = &explicit_type {
                    value.data.contrain_exact(t.deref());
                }
                match &name.data {
                    SymbolData::Value { r#type, .. } => {
                        r#type.connect(&value.data);
                    }
                    SymbolData::Type => todo!(),
                }
                data.contrain_exact(&types.unit)
            }
            ExprNodeK::Symbol(s) => match &s.data {
                SymbolData::Value { r#type, .. } => r#type.connect(&data),
                SymbolData::Type => todo!("{s:?}"),
            },
            ExprNodeK::Block(stmts) => {
                if let Some(l) = stmts.last() {
                    l.data.connect(&data);
                }
            }
            ExprNodeK::Invoke {
                function: _,
                arguments: _,
            } => {
                data.contrain_exact(&types.unit);
            }
            ExprNodeK::If {
                condition,
                cond_true,
                cond_false,
            } => {
                condition.data.contrain_exact(&types.bool);
                cond_false.data.connect(&data);
                cond_true.data.connect(&data);
            }
            ExprNodeK::While { condition, body: _ } => {
                condition.data.contrain_exact(&types.bool);
            }
            ExprNodeK::Literal(lit) => data.contrain_exact(&match lit {
                Literal::Int(_) => types.i32.clone(),
                Literal::Float(_) => types.f32.clone(),
                Literal::String(_) => todo!(),
            }),
            ExprNodeK::BinaryOp { lhs, rhs, .. } => {
                data.connect(&lhs.data);
                lhs.data.connect(&rhs.data);
            }
        });
        if unchanged {
            break;
        }
        if !CHANGED.load(Ordering::Relaxed) {
            unchanged = true;
        }
        CHANGED.store(false, Ordering::Relaxed);
    }
    Ok(ast)
}

impl TypeSlot {
    pub fn contrain_exact(&self, tt: &TypeA<Symbol>) {
        let mut t = self.0.write().unwrap();
        match t.deref_mut() {
            Some(x) => assert_eq!(x, tt, "types incompatible"),
            None => {
                *t = Some(tt.clone());
                CHANGED.store(true, Ordering::Relaxed)
            }
        }
    }
    pub fn connect(&self, rhs: &Self) {
        let mut a = self.0.write().unwrap();
        let mut b = rhs.0.write().unwrap();

        match (a.deref_mut(), b.deref_mut()) {
            (Some(a), Some(b)) => assert_eq!(a, b),
            (a @ None, Some(b)) => {
                *a = Some(b.clone());
                CHANGED.store(true, Ordering::Relaxed)
            }
            (Some(a), b @ None) => {
                *b = Some(a.clone());
                CHANGED.store(true, Ordering::Relaxed)
            }
            (None, None) => (),
        }
    }
    pub fn size(&self) -> usize {
        // match self.0.read().unwrap().as_ref().unwrap().as_str() {
        //     "()" | "¹" => 0,
        //     "i8" | "u8" => 1,
        //     "i16" | "u16" => 2,
        //     "i32" | "u32" => 4,
        //     "i64" | "u64" => 8,
        //     "i128" | "u128" => 16,
        //     "bool" => 1,
        //     x => unimplemented!("type {x:?} is unknown"),
        // }
        1
    }
}
