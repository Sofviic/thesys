use crate::{
    ast::{ExprNode, ExprNodeK, TypeA},
    code::ValueSlot,
    types::TypeSlot,
};
use anyhow::{anyhow, Result};
use std::{
    collections::HashMap,
    fmt::{Debug, Display, Formatter},
    hash::Hash,
    sync::{
        atomic::{AtomicU64, Ordering},
        Arc,
    },
};

static SYMBOL_ID_COUNTER: AtomicU64 = AtomicU64::new(0);

pub fn resolve_symbols(
    a: ExprNode<(), String>,
    globals: &HashMap<String, Symbol>,
) -> Result<ExprNode<(), Symbol>> {
    fn resolve_inner_type(a: TypeA<String>, scope: &mut SymbolStack) -> Result<TypeA<Symbol>> {
        Ok(TypeA {
            name: scope
                .get(&a.name)
                .ok_or(anyhow!("could not resolve type symbol {:?}", a.name))?,
            args: a
                .args
                .into_iter()
                .map(|a| resolve_inner_type(a, scope))
                .try_collect()?,
        })
    }
    fn resolve_inner(
        a: ExprNode<(), String>,
        scope: &mut SymbolStack,
    ) -> Result<ExprNode<(), Symbol>> {
        Ok(match a.k {
            ExprNodeK::Let {
                name,
                explicit_type,
                value,
            } => ExprNode {
                data: a.data,
                k: ExprNodeK::Let {
                    name: scope.create_value(name),
                    explicit_type: explicit_type
                        .map(|t| Ok::<_, anyhow::Error>(Box::new(resolve_inner_type(*t, scope)?)))
                        .transpose()?,
                    value: Box::new(resolve_inner(*value, &mut scope.create_child())?),
                },
            },
            ExprNodeK::Symbol(name) => ExprNode {
                data: a.data,
                k: ExprNodeK::Symbol(
                    scope
                        .get(name.as_str())
                        .ok_or(anyhow!("could not resolve symbol {name}"))?,
                ),
            },
            ExprNodeK::Block(content) => {
                let mut scope = scope.create_child();
                ExprNode {
                    data: a.data,
                    k: ExprNodeK::Block(
                        content
                            .into_iter()
                            .map(|n| resolve_inner(n, &mut scope))
                            .try_collect()?,
                    ),
                }
            }
            ExprNodeK::Invoke {
                function,
                arguments,
            } => ExprNode {
                data: a.data,
                k: ExprNodeK::Invoke {
                    function: Box::new(resolve_inner(*function, scope)?),
                    arguments: arguments
                        .into_iter()
                        .map(|n| resolve_inner(n, scope))
                        .try_collect()?,
                },
            },
            ExprNodeK::If {
                condition,
                cond_true,
                cond_false,
            } => ExprNode {
                data: a.data,
                k: ExprNodeK::If {
                    condition: Box::new(resolve_inner(*condition, &mut scope.create_child())?),
                    cond_true: Box::new(resolve_inner(*cond_true, &mut scope.create_child())?),
                    cond_false: Box::new(resolve_inner(*cond_false, &mut scope.create_child())?),
                },
            },
            ExprNodeK::While { condition, body } => ExprNode {
                data: a.data,
                k: ExprNodeK::While {
                    condition: Box::new(resolve_inner(*condition, &mut scope.create_child())?),
                    body: Box::new(resolve_inner(*body, &mut scope.create_child())?),
                },
            },
            ExprNodeK::Literal(lit) => ExprNode {
                data: a.data,
                k: ExprNodeK::Literal(lit),
            },
            ExprNodeK::BinaryOp { op, lhs, rhs } => ExprNode {
                data: a.data,
                k: ExprNodeK::BinaryOp {
                    op,
                    lhs: Box::new(resolve_inner(*lhs, &mut scope.create_child())?),
                    rhs: Box::new(resolve_inner(*rhs, &mut scope.create_child())?),
                },
            },
        })
    }

    let mut scope = SymbolStack::new(globals);
    resolve_inner(a, &mut scope)
}

#[derive(Debug, Clone)]
pub struct Symbol {
    pub id: u64,
    pub name: Arc<String>,
    pub data: SymbolData,
}

#[derive(Debug, Clone)]
pub enum SymbolData {
    Value { r#type: TypeSlot, value: ValueSlot },
    Type,
}

impl Symbol {
    pub fn new_value(name: &str) -> Self {
        Symbol {
            name: name.to_owned().into(),
            id: SYMBOL_ID_COUNTER.fetch_add(1, Ordering::Relaxed),
            data: SymbolData::Value {
                r#type: Default::default(),
                value: Default::default(),
            },
        }
    }
    pub fn new_type(name: &str) -> Self {
        Symbol {
            name: name.to_owned().into(),
            id: SYMBOL_ID_COUNTER.fetch_add(1, Ordering::Relaxed),
            data: SymbolData::Type,
        }
    }
}
impl Display for Symbol {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}~{}{}",
            self.name,
            self.id,
            match &self.data {
                SymbolData::Type => "".to_string(),
                SymbolData::Value { r#type, .. } => {
                    r#type
                        .0
                        .read()
                        .unwrap()
                        .clone()
                        .map(|s| format!(" :: {s}"))
                        .unwrap_or_default()
                }
            }
        )
    }
}
impl Hash for Symbol {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}
impl PartialEq for Symbol {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}
impl Eq for Symbol {}

struct SymbolStack<'a> {
    parent: Option<&'a SymbolStack<'a>>,
    symbols: HashMap<String, Symbol>,
}
impl<'a> SymbolStack<'a> {
    fn new(globals: &HashMap<String, Symbol>) -> Self {
        Self {
            parent: None,
            symbols: globals.to_owned(),
        }
    }
    pub fn create_value(&mut self, name: String) -> Symbol {
        let symbol = Symbol::new_value(&name);
        self.symbols.insert(name, symbol.clone());
        symbol
    }
    pub fn get(&self, name: &str) -> Option<Symbol> {
        self.symbols
            .get(name)
            .cloned()
            .or_else(|| self.parent.map(|p| p.get(name)).flatten())
    }
    pub fn create_child(&'a self) -> Self {
        Self {
            parent: Some(self),
            symbols: HashMap::default(),
        }
    }
}
