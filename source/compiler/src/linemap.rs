use std::fmt::Display;

#[derive(Debug, Clone, Copy)]
pub struct SourceStr<'a> {
    pub span: &'a str,
    linemap: &'a Linemap,
    offset: usize,
}
impl<'a> SourceStr<'a> {
    pub fn new(span: &'a str, linemap: &'a Linemap) -> Self {
        Self {
            span,
            offset: 0,
            linemap,
        }
    }
    pub fn split_at(self, p: usize) -> (Self, Self) {
        let (a, b) = self.span.split_at(p);
        (
            Self {
                linemap: self.linemap,
                span: a,
                offset: self.offset,
            },
            Self {
                linemap: self.linemap,
                offset: self.offset + p,
                span: b,
            },
        )
    }
    pub fn trim_start(self) -> Self {
        let s = self.span.trim_start();
        Self {
            span: s,
            linemap: self.linemap,
            offset: self.offset + self.span.len() - s.len(),
        }
    }
    pub fn len(&self) -> usize {
        self.span.len()
    }
    pub fn err_maxlen(self, l: usize) -> Self {
        Self {
            linemap: self.linemap,
            offset: self.offset,
            span: &self.span[..l.min(self.span.len())],
        }
    }
}
impl Display for SourceStr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let span = self.span;
        let (line, col) = self.linemap.map(self.offset);
        write!(f, "{span:?}[{line}:{col}]")
    }
}

#[derive(Debug)]
pub struct Linemap(Vec<usize>);

impl Linemap {
    pub fn new(s: &str) -> Self {
        let mut o = Vec::new();
        for (pos, c) in s.char_indices() {
            if c == '\n' {
                o.push(pos)
            }
        }
        Self(o)
    }
    pub fn map(&self, p: usize) -> (usize, usize) {
        let mut l = (0, 0);
        for (i, pk) in self.0.iter().enumerate() {
            if *pk > p {
                break;
            }
            l = (i + 1, *pk);
        }
        (l.0 + 1, p - l.1 + 1)
    }
}
