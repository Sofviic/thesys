use crate::{
    ast::{BinaryOp, Declaration, ExprNode, ExprNodeK, Function, Literal, TypeA},
    linemap::SourceStr,
};
use anyhow::{anyhow, bail, Result};
use std::sync::{Arc, LazyLock};

pub type Parser<T> = Box<dyn Fn(SourceStr) -> Result<(SourceStr, T, Option<anyhow::Error>)>>;
pub type ParserFactory<T> = Box<dyn Fn() -> Parser<T>>;

pub fn parse_file() -> Parser<Vec<Declaration<(), String>>> {
    seq(
        many(seq(opt_whitespace(), parse_fn_decl()).second()),
        opt_whitespace(),
    )
    .first()
}

pub fn parse_fn_decl() -> Parser<Declaration<(), String>> {
    seq(
        seq(exact("fn".to_owned()), whitespace()),
        seq(
            ident(),
            seq(
                exact("(".to_string()),
                seq(
                    many_sep(
                        seq(
                            ident(),
                            seq(
                                exact(":".to_string()),
                                seq(opt_whitespace(), lazy(parse_type)).second(),
                            )
                            .second(),
                        ),
                        exact(",".to_string()),
                    ),
                    seq(
                        exact(")".to_string()),
                        seq(opt_whitespace(), parse_block()).second(),
                    )
                    .second(),
                ),
            )
            .second(),
        ),
    )
    .second()
    .map(|(name, (arguments, body))| {
        Declaration::Function(Function {
            name,
            arguments,
            body,
        })
    })
}

fn parse_node() -> Parser<ExprNode<(), String>> {
    seq(
        opt_whitespace(),
        seq(
            alt(
                lazy(parse_block),
                alt(
                    lazy(parse_let),
                    alt(
                        lazy(parse_invoke),
                        alt(parse_literal_number(), alt(parse_symbol(), lazy(parse_bop))),
                    ),
                ),
            ),
            opt_whitespace(),
        ),
    )
    .second()
    .first()
}

fn parse_bop() -> Parser<ExprNode<(), String>> {
    seq(
        exact_alts(&["+", "-", "*", "/", "%"]),
        // TODO fix whitespace
        // seq(
        //     whitespace(),
        //     seq(parse_node(), seq(whitespace(), parse_node()).second()),
        // )
        // .second(),
        seq(parse_node(), parse_node()),
    )
    .map(|(op, (lhs, rhs))| ExprNode {
        data: (),
        k: ExprNodeK::BinaryOp {
            op: match op {
                "+" => BinaryOp::Add,
                "-" => BinaryOp::Sub,
                "*" => BinaryOp::Mul,
                "/" => BinaryOp::Div,
                "%" => BinaryOp::Mod,
                _ => unreachable!(),
            },
            lhs: Box::new(lhs),
            rhs: Box::new(rhs),
        },
    })
}

fn parse_symbol() -> Parser<ExprNode<(), String>> {
    ident().map(|name| ExprNode {
        data: (),
        k: ExprNodeK::Symbol(name),
    })
}

fn parse_invoke() -> Parser<ExprNode<(), String>> {
    seq(
        ident(),
        seq(
            exact("(".to_string()),
            seq(
                many_sep(parse_node(), exact(",".to_string())),
                exact(")".to_string()),
            )
            .first(),
        )
        .second(),
    )
    .map(|(fname, arguments)| ExprNode {
        data: (),
        k: ExprNodeK::Invoke {
            function: Box::new(ExprNode {
                data: (),
                k: ExprNodeK::Symbol(fname),
            }),
            arguments,
        },
    })
}

fn parse_literal_number() -> Parser<ExprNode<(), String>> {
    signed_integer().map(|n| ExprNode {
        data: (),
        k: ExprNodeK::Literal(Literal::Int(n)),
    })
}

fn parse_let() -> Parser<ExprNode<(), String>> {
    seq(
        exact("let ".to_string()),
        seq(
            ident(),
            seq(
                opt(seq(
                    exact(":".to_string()),
                    seq(opt_whitespace(), parse_type().map(Box::new)).second(),
                )
                .second()),
                seq(
                    opt_whitespace(),
                    seq(exact("=".to_string()), parse_node().map(Box::new)).second(),
                )
                .second(),
            ),
        ),
    )
    .second()
    .map(|(name, (explicit_type, value))| ExprNode {
        data: (),
        k: ExprNodeK::Let {
            name,
            explicit_type,
            value,
        },
    })
}

fn parse_type() -> Parser<TypeA<String>> {
    ident().map(|name| TypeA { name, args: vec![] })
}

fn parse_block() -> Parser<ExprNode<(), String>> {
    seq(
        exact("{".to_string()),
        seq(
            many_sep(parse_node(), exact(";".to_owned())),
            exact("}".to_string()),
        ),
    )
    .second()
    .first()
    .map(|nodes| ExprNode {
        data: (),
        k: ExprNodeK::Block(nodes),
    })
}

fn ident() -> Parser<String> {
    Box::new(move |s| {
        let mut k = 0;
        for (i, c) in s.span.char_indices() {
            if !c.is_alphanumeric() && c != '_' {
                k = i;
                break;
            }
        }
        if k == 0 {
            bail!("expected identifier")
        }
        let (a, b) = s.split_at(k);
        Ok((b, a.span.to_string(), None))
    })
}
fn unsigned_integer() -> Parser<u64> {
    Box::new(move |s| {
        let mut k = 0;
        for (i, c) in s.span.char_indices() {
            if !c.is_numeric() && c != '_' {
                k = i;
                break;
            }
        }
        if k < 1 {
            bail!("expected integer")
        }
        let (a, b) = s.split_at(k);
        Ok((b, a.span.replace("_", "").parse().unwrap(), None))
    })
}
fn signed_integer() -> Parser<i64> {
    seq(opt(exact("-".to_string())), unsigned_integer())
        .map(|(sign, num)| num as i64 * if sign.is_some() { -1 } else { 1 })
}

fn lazy<T: 'static>(pg: impl FnOnce() -> Parser<T> + 'static) -> Parser<T> {
    let l = LazyLock::new(pg);
    Box::new(move |s| l(s))
}

fn opt_whitespace() -> Parser<()> {
    Box::new(move |s| {
        let t = s.trim_start();
        Ok((t, (), None))
    })
}
fn whitespace() -> Parser<()> {
    Box::new(move |s| {
        let t = s.trim_start();
        if t.len() == s.span.len() {
            bail!("expected whitespace")
        }
        Ok((t, (), None))
    })
}

fn alt<T: 'static>(a: Parser<T>, b: Parser<T>) -> Parser<T> {
    Box::new(move |s| match a(s) {
        Ok(x) => Ok(x),
        Err(a_err) => match b(s) {
            Ok(x) => Ok(x),
            Err(b_err) => Err(anyhow!("{a_err} or {b_err}")),
        },
    })
}
fn seq<A: 'static, B: 'static>(a: Parser<A>, b: Parser<B>) -> Parser<(A, B)> {
    Box::new(move |s| {
        let (s, av, ae) = a(s)?;
        let (s, bv, be) = b(s)?;
        Ok((s, (av, bv), be.or(ae)))
    })
}
fn opt<T: 'static>(a: Parser<T>) -> Parser<Option<T>> {
    Box::new(move |s| match a(s) {
        Ok((rest, value, e)) => Ok((rest, Some(value), e)),
        Err(e) => Ok((s, None, Some(e))),
    })
}
fn many<T: 'static>(p: Parser<T>) -> Parser<Vec<T>> {
    Box::new(move |mut s| {
        let mut values = Vec::new();
        loop {
            match p(s) {
                // TODO handle err here?
                Ok((rest, v, _e)) => {
                    s = rest;
                    values.push(v);
                }
                Err(e) => return Ok((s, values, Some(e))),
            }
        }
    })
}
fn many_sep<T: 'static, U: 'static>(p: Parser<T>, sep: Parser<U>) -> Parser<Vec<T>> {
    let p = make_factory(p);
    opt(
        seq(p(), opt(many(seq(sep, p()).second()))).map(|(first, rest)| {
            let mut v = rest.unwrap_or_default();
            v.insert(0, first);
            v
        }),
    )
    .map(|e| e.unwrap_or_default())
}
fn exact(pat: String) -> Parser<String> {
    Box::new(move |s| {
        if s.span.starts_with(&pat) {
            let (_, s) = s.split_at(pat.len());
            Ok((s, pat.clone(), None))
        } else {
            bail!("expected {pat:?} found {}", &s.err_maxlen(pat.len()))
        }
    })
}
pub fn make_factory<T: 'static>(p: Parser<T>) -> ParserFactory<T> {
    let p = Arc::new(p);
    Box::new(move || {
        let p = p.clone();
        Box::new(move |s| p(s))
    })
}
fn exact_alts(pats: &'static [&'static str]) -> Parser<&'static str> {
    Box::new(move |s| {
        for pat in pats {
            if s.span.starts_with(pat) {
                let (_, s) = s.split_at(pat.len());
                return Ok((s, pat, None));
            }
        }
        bail!("expected one of {pats:?} found {}", &s.err_maxlen(10))
    })
}

pub trait ParserMapExt {
    type T;
    fn map<U: 'static, F: 'static + Fn(Self::T) -> U>(self, f: F) -> Parser<U>;
}
impl<T: 'static> ParserMapExt for Parser<T> {
    type T = T;
    fn map<U: 'static, F: 'static + Fn(Self::T) -> U>(self, f: F) -> Parser<U> {
        Box::new(move |s| {
            let (s, v, e) = self(s)?;
            Ok((s, f(v), e))
        })
    }
}

pub trait ParserTupleExt {
    type A;
    type B;
    fn first(self) -> Parser<Self::A>;
    fn second(self) -> Parser<Self::B>;
}
impl<A: 'static, B: 'static> ParserTupleExt for Parser<(A, B)> {
    type A = A;
    type B = B;
    fn first(self) -> Parser<Self::A> {
        self.map(|(a, _b)| a)
    }
    fn second(self) -> Parser<Self::B> {
        self.map(|(_a, b)| b)
    }
}
