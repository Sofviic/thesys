use crate::instructions::{Argument, Instruction};
use anyhow::{anyhow, bail, Context};

pub fn parse_assembly(s: &str) -> anyhow::Result<Vec<Instruction>> {
    let mut out = Vec::new();
    for (linum, line) in s.lines().enumerate() {
        parse_line(&mut out, line).context(anyhow!("line {linum}"))?;
    }
    Ok(out)
}

fn parse_line(out: &mut Vec<Instruction>, line: &str) -> anyhow::Result<()> {
    if line.starts_with("#") || line.is_empty() {
        return Ok(());
    }

    let line = line.trim();
    // if let Some(label) = line.strip_prefix("_") {
    //     out.push(Instruction::_Label(Label(label.to_string())));
    //     return Ok(());
    // }
    let (line, _) = line.split_once("#").unwrap_or((line, ""));

    let mut tokens = line.split(" ");

    let mnemonic = tokens.next().unwrap();
    eprintln!("{mnemonic:?}");
    let x = tokens
        .next()
        .map(parse_arg)
        .transpose()
        .context("first argument")
        .map(|e| e.ok_or(anyhow!("no first argument")))
        .flatten();
    let y = tokens
        .next()
        .map(parse_arg)
        .transpose()
        .context("second argument")
        .map(|e| e.ok_or(anyhow!("no second argument")))
        .flatten();
    let z = tokens
        .next()
        .map(parse_arg)
        .transpose()
        .context("third argument")
        .map(|e| e.ok_or(anyhow!("no third argument")))
        .flatten();

    out.push(match mnemonic.to_uppercase().as_str() {
        "NOA" => Instruction::Noa,
        "ADD" => Instruction::Add(x?, y?, z?),
        "SUB" => Instruction::Sub(x?, y?, z?),
        "MUL" => Instruction::Mul(x?, y?, z?),
        "DIV" => Instruction::Div(x?, y?, z?),
        "MOD" => Instruction::Mod(x?, y?, z?),
        "EQUAL" => Instruction::Equal(x?, y?, z?),
        "LESS" => Instruction::Less(x?, y?, z?),
        "GREATER" => Instruction::Greater(x?, y?, z?),
        "LNOT" => Instruction::Lnot(x?, y?),
        "BNOT" => Instruction::Bnot(x?, y?),
        "XOR" => Instruction::Xor(x?, y?, z?),
        "OR" => Instruction::Or(x?, y?, z?),
        "AND" => Instruction::And(x?, y?, z?),
        "SHL" => Instruction::Shl(x?, y?, z?),
        "SHR" => Instruction::Shr(x?, y?, z?),
        "MOVE" => Instruction::Move(x?, y?),
        "RSP" => Instruction::Rsp(x?),
        "PUSH" => Instruction::Push(x?),
        "POP" => Instruction::Pop(x?),
        "JUMP" => Instruction::Jump(x?),
        "CJUMP" => Instruction::Cjump { cond: x?, pc: y? },
        "RPC" => Instruction::Rpc(x?),
        "SPAWN" => Instruction::Spawn { pc: x?, sp: y? },
        "HALT" => Instruction::Halt,
        "ACCEPT" => Instruction::Accept { id: x? },
        "READ" => Instruction::Read {
            id: x?,
            value: y?,
            error: z?,
        },
        "WRITE" => Instruction::Write {
            id: x?,
            value: y?,
            error: z?,
        },
        _ => bail!("what is {mnemonic}?"),
    });
    Ok(())
}

fn parse_arg(s: &str) -> anyhow::Result<Argument> {
    Ok(
        //     if s.starts_with("i@") {
        //     Argument::_LabelImmediate(Label(s[2..].to_string()))
        // } else
        if s.starts_with("i'") {
            Argument::Immediate(s.chars().nth(2).ok_or(anyhow!("no char"))? as u8 as i64)
        } else {
            eprintln!("{:?}", s);
            if s.len() < 2 {
                bail!("argument too short")
            }
            let n = s[1..].parse()?;
            match s.chars().nth(0).ok_or(anyhow!("no char"))? {
                'i' => Argument::Immediate(n),
                'm' => Argument::Pointer(n),
                's' => Argument::Stack(n),
                'a' => Argument::StackPointer(n),
                _ => unreachable!(),
            }
        },
    )
}
