use anyhow::anyhow;
use futures_util::{SinkExt, StreamExt};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
    sync::Semaphore,
};
use warp::{
    filters::ws::{Message, WebSocket},
    Filter,
};

static CONN_LIMIT: Semaphore = Semaphore::const_new(32);

#[tokio::main]
async fn main() {
    let websocket = warp::path("device")
        .and(warp::ws())
        .map(|ws: warp::ws::Ws| {
            ws.on_upgrade(|websocket| async move {
                let Ok(_permit) = CONN_LIMIT.try_acquire() else {
                    return;
                };

                if let Err(e) = handle(websocket).await {
                    eprintln!("{e:?}")
                }

                drop(_permit);
                return;
            })
        });

    let index = warp::path!().map(|| {
        warp::reply::with_header(
            include_str!("terminal.html"),
            // std::fs::read_to_string("devices/terminal/src/terminal.html").unwrap(),
            "content-type",
            "text/html",
        )
    });
    let script = warp::path!("terminal.js").map(|| {
        warp::reply::with_header(
            include_str!("terminal.js"),
            // std::fs::read_to_string("devices/terminal/src/terminal.js").unwrap(),
            "content-type",
            "application/javascript",
        )
    });

    warp::serve(index.or(script).or(websocket))
        .run(([127, 0, 0, 1], 8000))
        .await;
}

async fn handle(websocket: WebSocket) -> anyhow::Result<()> {
    let mut socket = TcpStream::connect("127.0.0.1:2000").await?;
    let (mut orx, mut otx) = socket.split();
    let (mut tx, mut rx) = websocket.split();

    loop {
        let mut b = [0u8];
        tokio::select! {
            r = orx.read_exact(&mut b) => {
                r?;
                tx.send(Message::text(char::from_u32(b[0] as u32).ok_or(anyhow!("invalid char"))?))
                    .await?;
            },
            r = rx.next() => {
                let p = r.ok_or(anyhow!("stream end"))??;
                let p = p.to_str().map_err(|_| anyhow!("ws non string"))?;
                otx.write_all(p.as_bytes()).await?;
            }
        };
    }
}
