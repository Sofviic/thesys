#import "@preview/cetz:0.2.1"
#import "common.typ": todo, logoshow
#set heading(numbering: "1.")
#set text(font: "New Computer Modern")
#set document(title: "Thesys")
#show "Thesys": logoshow

// #align(center, text(size: 24pt, [Thesys _Experiment_: VLIW]))
#align(center, text(size: 24pt, [Title goes here]))
#align(center, [by metamuffin])
#pad(x: 30pt, outline())

#let bitcount = counter("bits")
#let buffercount = counter("buffers")
#let compcount = counter("comps")

= Introduction

Current CPUs achieve faster computing speeds by parallely executing instructions with pipelining, speculative execution and SIMD.
This allows the CPU to utilize all internal components at all times, increasing speed if they do useful work. Pipelining and speculative execution especially are prone to hardware bugs and introduce great complexity into the CPU and should therefore be avoided //for Thesys.

_Very Long Instruction Word_ (VLIW) architectures don't perform these optimizations at runtime, elimnating the risk of error (at least in hardware), while concurrently utilizing all components.

= Proposed Architecture

Every clock cycle the CPU loads one instruction that specifies operation of every component during that cycle.
Every component has a buffered output for using the result in the next cycle. Inputs of the components are taken directly from the output buffer of the previous cycle.

== Components

When using all #locate(loc => compcount.final(loc).at(0)) components, the size of one instruction word needs to be at least #locate(loc => bitcount.final(loc).at(0)) bits.
There will be #locate(loc => buffercount.final(loc).at(0)) addressable output buffers (#(locate(loc => (
  calc.ceil(calc.log(buffercount.final(loc).at(0), base: 2))
)))-bit address).

#let k = 4
#let imm = 32

#let components(cb) = {
  cb(name: [Zero Constant], outs: ([Zero],))
  cb(name: [One Constant], outs: ([One],))
  
  cb(name: [ALU: Add/Sub], ins: (
    (w: 1, k: [subtract mode]),
    (w: k, k: [Left side]),
    (w: k, k: [Right side]),
  ), outs: ([Sum],[Is Zero], [Is Positive]))

  cb(name: [ALU: Mul/Div], ins: (
    (w: 1, k: [div mode]),
    (w: k, k: [Left side]),
    (w: k, k: [Right side]),
  ), outs:([Product],))

  cb(name: [ALU: Float Add/Sub], ins: (
    (w: 1, k: [subtract mode]),
    (w: k, k: [Left side]),
    (w: k, k: [Right side]),
  ), outs: ([Sum],), lag: 2)

  cb(name: [ALU: Float Mul/Div],ins: (
    (w: 1, k: [div mode]),
    (w: k, k: [Left side]),
    (w: k, k: [Right side]),
  ), outs: ([Product],), lag: 2)

  cb(name: [ALU: Binary operations],ins: (
    (w: 3, k: [mode: Shift Left / Shift Right / Or / And / Xor / Not]),
    (w: k, k: [Left side]),
    (w: k, k: [Right side]),
  ), outs: ([Result],))

  for i in ("A", "B", "C", "D", "E", "F") {
    cb(name: [Register #i],ins: ((w: k, k: [Value]),), outs: ([Previous Value],))
  }
  for i in ("A", "B") {
    cb(name: [Memory Loader #i], ins: ((w: k, k: [Address]),), outs: ([Value],), lag: 4)
  }
  cb(name: [Memory Storer], ins: (
    (w: k, k: [Address]),
    (w: k, k: [Value]),
  ))

  cb(name: [Instruction Immediate Argument], lag: 0, ins: (
    (w:imm, k: [Value]),
  ), outs: ([Value],))

  cb(name: [Flow controller],ins: (
    (w: k, k: [Jump Address]),
    (w: k, k: [Jump Condition]),
  ))
}

#components((name: (),ins: (),outs: (), lag: 1) => [
  === #name
  #compcount.step()
  #if ins.len() > 0 { box(fill: rgb("00ff0040"), inset: 5pt, [
    Parameters:
    #for i in ins [
      - #(i.w)-bit #i.k
      #bitcount.update(x => x + i.w)
    ]
  ])}
  #if outs.len() > 0 { box(fill: rgb("4000f040"), inset: 5pt, [
    Output buffers:
    #for o in outs [
      - #o
      #buffercount.step()
    ]
  ])}
  #if lag != 1 { box(fill: rgb("ff000040"), inset: 5pt, [
    #if lag < 1 [No output lag] else if lag > 1 [Output lag: #lag cycles]
  ])}
])

== Instruction Binary Format


#let bc = counter("ibf_bits")
#components((name: (), ins: (), outs: (), lag: 0) => stack(
  dir: ttb,
  rect(fill: rgb("00000020"), stack(dir: ltr, 
    rect(stroke: none, width: 200pt, name),
    stack(dir: ttb, for i in ins {
      rect(fill: rgb("00ff0040"), stroke: black, height: calc.max(17pt, i.w * 10pt), width: 200pt, if i.w == 1 [
        Bit #bc.display() #bc.update(x => x + i.w): #i.k
      ] else [
        Bits #bc.display() - #bc.update(x => x + i.w) #bc.display(): #i.k
      ])
    })
  ))
))
