#import "common.typ": todo, logoshow
#set heading(numbering: "1.")
#set text(font: "New Computer Modern")
#set document(title: "Thesys")
#show "Thesys": logoshow

#align(center, text(size: 24pt, "Thesys"))
#align(center, [by metamuffin])
#pad(x: 30pt, outline())

= Introduction

#todo("Einleitung: Warum überhaupt bootstrappen?")

= Processor Architecture

Thesys uses a minimal instruction set with low-level support for external communications. 
A stack is used to store intermediate results.

== Functional Description

The Processor has a number of _Cores_ which execute instructions sequencially according to a program and _Memory_ which stores addressable 64-bit integer numbers (_Values_). 
The memory has a fixed size. This manifests through a maximum address after which the memory is not writeable.
All cores execute concurrently -- their syncronization is undefined, they may execute at different rates.

Each core has three internal registers: _Stack Pointer_ (SP), _Base Pointer_ (BP) and _Program Counter_ (PC).
These cores repeatedly load an instruction (one value) from memory where PC points.
Hereby the number of cores is not fixed during runtime.
Boolean operations treat 0 as false and all other numbers as true.

After power-up the processor starts with a single core with $"PC" = 0$ and SP at highest address supported by the current memory.
The memory is preloaded with an initial bootloader (see @bootloader).

=== Instruction Binary Format

On each clock cycle the Processor will read Memory at $"PC"$. The value found will be decoded into _Opcode_ and _Addressing Modes_ of all arguments and the according number of arguments will be read; further incrementing the $"PC"$. Then the instruction is executed according to @instructions where $x$, $y$ and $z$ are arguments 1 through 3 in order.

Opcode and Addressing Mode are packed into a single value in the following way:  $I = "Opcode" | ("AM"_1 << 8) | ("AM"_2 << 10) | ("AM"_3 << 12) | ("AM"_4 << 14) | ...$

=== Addressing Modes <addressingmodes>

Every argument ($x$) can be intepreted according to one of four addressing modes:
1. Immediate (`i`): $x$
2. Base relative memory pointer (`b`): $M("BP"+x)$
3. Stack relative pointer (`s`): $M("SP"+x)$
4. Stack relative memory pointer pointer™ (`a`): $M(M("SP"+x))$

#let ic = counter("inst_opcode")
#let c()=[#ic.display() #ic.step()]
#figure(
  caption: [The Thesys Instruction Set],
  table(
    columns: 5,
    [],[*Opcode*], [*Mnemonic*], [*Action*], [*Description*],
    [],c(),[`noa`], [],[No action],
    [*Arithmetic*],c(),[`add`],[$z = x+y$],[Addition],
    [],c(),[`sub`], [$z = x-y$],[Subtractions],
    [],c(),[`mul`], [$z = x*y$],[Multiplication],
    [],c(),[`div`], [$z = x/y$], [Integer Division],
    [],c(),[`equal`], [$z = cases(1 quad x = y,0 quad "otherwise")$],[Compare if equal],
    [],c(),[`less`], [$z = cases(1 quad x < y,0 quad "otherwise")$],[Compare if less],
    [],c(),[`greater`], [$z = cases(1 quad x > y,0 quad "otherwise")$],[Compare if greater],
    [],c(),[`lnot`], [$y = not x$],[Logical not],
    [],c(),[`bnot`], [$y = ~x$],[Binary not],
    [],c(),[`xor`], [$z = x xor y$],[Binary (and also logical) xor],
    [],c(),[`or`], [$z = x and y$],[Binary (and also logical) or],
    [],c(),[`and`], [$z = x or y$],[Binary (and also logical) and],
    [],c(),[`shl`], [$z = x << y$],[Bit shift left],
    [],c(),[`shr`], [$z = x >> y$],[Bit shift right],
    [*Memory*],c(),[`move`],[$y = x$], [Move a value],
    [],c(),[`rsp`],[$x = "SP"$], [Read stack pointer],
    [],c(),[`push`],[$"SP"' = "SP" - x$], [Move stack pointer up],
    [],c(),[`pop`],[$"SP"' = "SP" + x$], [Move stack pointer down],
    [*Control flow*],c(),[`jump`],[$"PC" = x$],[ Store program counter],
    [],c(),[`cjump`], [$"PC"' = cases(y quad x!=0,"PC" quad x=0)$],[Conditional jump],
    [],c(),[`rpc`],[$x = "PC"$], [Read program counter],
    [],c(),[`spawn`],[], [Creates a new core with $"PC" = x, "SP" = y$],
    [],c(),[`halt`],[],[Removes this core],
    [*Input / Output*],c(),[`accept`],[$x = "ID"$],[ Waits for a new device. Returns identifier for that device.],
    [],c(),[`read`],[$y,z= "Read"(x)$],[Wait for new data from device $x$, reads a single value $y$ with error flag $z$],
    [],c(),[`write`],[$z="Write"(x,y)$],[Send $y$ to device $x$; May block],
  )
) <instructions>

== Hardware implementation

#todo[...]

// (ka, ob das sinn macht): In actual non-emulated hardware multi-core may be implemented to some extent using multiple processors, SMT when using instruction-level parallelism and interrupts for halting dormant threads.

= Operating System

== Assembly Notation

Instructions are noted with their mnemonic followed by all arguments prefixed with their addressing mode `i`, `s`, `m` or `a` like in @addressingmodes. Comments are prepended with `//`.

#figure(
  caption: "Example Assembly Program",
  ```as
  // Accept a device and enumerate all bytes
  push 2
  move i0 s1
  accept s0
  write s0 s1
  add s1 i1 s1
  jump i7
  ```
)

== Calling convention

#figure(
  table(columns: 1,
    pad(y:15pt)[Locals of Function],
    pad(y:5pt)[Arguments of Function],
    pad(y:5pt)[Return values of Function],
    pad(y:-2pt)[Return PC],
    pad(y:15pt)[Locals of Caller],
  ),
  caption: [Stack diagram of the calling convention],
  supplement: "Diagram"
)

== Bootstrapping Bootloader <bootloader>

The _Bootloader_ is what the processor initially starts execution with.
It accepts new devices and waits for them to specify their type by sending a single value.
Then an entry-point is executed according to a global look-up-table.
A single entry-point for type `A` is provided initially for writing new functions to memory. 

=== Implementation

#figure(```as
push i5 // Locals: Counter, Device ID, Type, Entry PC, New SP
move i0 s0 // init counter
accept s1 // accept device
read s1 s2 m1024 // read type
add s2 i1024 s3
move a3 s3
mul s1 i1024 s4 // calculate new SP
add s4 i8192 s4
move s1 a4 // store ID into new SP
spawn s3 s4 // create handler core
add i1 s0 // increment counter
jump i5 // jump right after init
// Jump mark: store address at 0x1000 + ord('A')
// basic assembler from here on
push i5
move i0 s0
move s0 s1
move i8 s2
// loopstart
read s5 s3 s4
lnot s4 s4
cjump s4 i<after halt>
halt
// after halt
equal s3 i'!' s4
cjump s4 i<exe>
less s3 i'`' s4
cjump s4 i<setam>
less s3 i':' s4
cjump s4 i<wdigit>
sub s3 i'A' a1
add s1 i1 s1
move i0 s2
jump i<loopstart>
// setam
sub s3 i'#' s3
shr s3 s2 s3
or a1 s3 a1
add s2 i2 s2
jump i<loopstart>
// wdigit
sub s3 i'0' s4
mul i10 a1 a1
add s4 a1 a1
add s1 i1 s1
jump i<loopstart>
// exe
move s5 s3
move i<loopstart> s4
jump s0
```, caption: [A basic bootloader for bootstrapping purposes])

#figure(
  caption: [Pseudo-code of the bootstrapping bootloader],
  ```py
  am_off = 8
  start = ??
  cur = start
  while(1) {
    c, r = read()
    if (!r) exit()
    if (c == '!') {
      (*start)()
    } else if (c < '´') { # for matching #"%$ 
      *cur |= (c - '#') << am_off
      am_off += 2
    } else if (c < ':') {
      *cur = c - '0'
      cur += 1
    } else {
      *cur = c - 'A'
      cur += 1
      am_off = 0
    }
  }
  ```
)

// #figure(```hs
// T5 -- Locals: Counter, Device ID, Type, Entry PC, New SP
// i0 S0 -- init counter
// a_ S1 -- accept device
// r1 S3 -- read type
// i4096 +3 m_ S4 -- read LUT
// i8192 S5 i1024 *0 +5 S5 -- calculate new SP
// s5 M1 -- store ID into new SP
// s5 f4 -- create handler
// i1 +0 -- increment counter
// J3 -- jump right after init
// ---- Jump mark: store address at 0x1000 + ord('A')
// T2 -- Locals: Bytes read, Current Byte, (Device ID)
// i0 S0 -- br = 0
// r2 S1 -- Read a byte
// ```, caption: [A basic bootloader for bootstrapping purposes (not-so-old stack-based ISA with acc)])

// #figure(```hs
// K a -- Accept device
// dr #4096+m -- Read type and load entry via LUT at 0x1000
// #1b #1024* #8192+ -- Calculate address for stack of handler 
// #0b #1b M -- Store device ID onto new stack
// fD -- Fork into handler;
// #1J -- Repeat forever
// ---- Jump mark: store address at 0x1000 + ord('A')
// K #0 -- locals
// #0!b r #0bk#512-+ M -- Store value 512 below SP
// #0b #1+ #0B
// #120= d(k#512-)#* #1s(#<jump mark>)* J -- On 'x' (120) start execution, otherwise repeat
// ```, caption: [A basic bootloader for bootstrapping purposes (old stack-based ISA)])

// #figure(
//   caption: [Old Instruction Table],
//   table(
//     columns: 5,
//     [], [*Opcode*], [*Pop*], [*Push*], [*Action*],
//     [*Arithmetic*],[`+`],[$x,y$], [$z$], [$z = x+y$; Addition],
//     [],[`-`],[$x,y$], [$z$], [$z = x-y$; Subtractions],
//     [],[`*`],[$x,y$], [$z$], [$z = x*y$; Multiplication],
//     [],[`/`],[$x,y$], [$q,r$], [$q = x/y, r = r mod y$; Integer Division],
//     [],[`=`],[$x,y$], [$z$], [Returns if $x = y$],
//     [],[`<`],[$x,y$], [$z$], [Returns if $x < y$],
//     [],[`>`],[$x,y$], [$z$], [Returns if $x > y$],
//     [],[`!`],[$x$], [$z$], [Returns $not x$],
//     [],[`~`],[$x$], [$z$], [Binary not],
//     [],[`^`],[$x,y$], [$z$], [$z= x xor y$; Binary (and also logical) xor],
//     [],[`&`],[$x,y$], [$z$], [$z= x and y$; Binary (and also logical) or],
//     [],[`|`],[$x,y$], [$z$], [$z= x or y$; Binary (and also logical) and],
//     [*Memory*],[`m`],[$a$], [$v$], [Load $v$ from memory address $a$],
//     [],[`M`],[$v,a$], [], [Store $v$ at memory address $a$],
//     [],[`s`],[$a$], [$v$], [Load $v$ from memory at $"SP" + a$],
//     [],[`S`],[$v,a$], [], [Store $v$ into memory at $"SP" + a$],
//     [],[`b`],[$a$], [$v$], [Load $v$ from memory at $"BP" + a$],
//     [],[`B`],[$v,a$], [], [Store $v$ into memory at $"BP" + a$],
//     [],[`d`],[$x$], [$x,x$], [Duplicate stack top],
//     [],[`D`],[$x$], [$x,x$], [Discard stack top],
//     [*Control flow*],[`J`],[$p$], [], [$"PC" = p$; Store program counter],
//     [],[`j`],[], [$p$], [$p = "PC"$; Read program counter],
//     [],[`k`],[], [$p$], [$p = "SP"$; Read stack pointer],
//     [],[`K`],[], [], [$"BP" = "SP"$; Set base],
//     [],[`f`],[$p, s$], [$k$], [Creates a new core with $"SP" = s, "BP" = s, "PC" = p$],
//     [],[`F`],[],[],[Removes this core],
//     [],[`(`,`)`,`_`],[],[],[No action],
//     [*Literals*],[`#`],[], [$n$], [Pushes zero],
//     [],[`0` - `9`],[$n'$], [$n$], [$n' = 10n + "opcode"$],
//     [*Input / Output*],[`A`],[],[$i$],[Accept: Waits for a new device. Returns identifier $i$ of that device.],
//     [],[`r`],[$i$],[$x$],[Read: Wait for new data from device $i$, return one value $x$],
//     [],[`w`],[$i,x$],[],[Write: Send $x$ to device $i$; May wait],
//   )
// )

// #counter(heading).update(0)
// #heading(numbering: "A.", supplement: "Appendix", "Appendix")

// #raw(read("../processor.c"), lang: "c")
