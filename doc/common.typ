

#let logoshow = x => box(text(
  font: "Ubuntu",
  fill: gradient.linear(rgb(200,0,100),rgb(100,0,200)),
  x
))

#let todo(x) = box(radius: 3pt, fill: red, pad(rest: 5pt, text(fill: white, [*To-do*: #x])))

