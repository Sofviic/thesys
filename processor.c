#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

typedef int64_t value;
enum opcode {
  OP_NOA,
  OP_ADD,
  OP_SUB,
  OP_MUL,
  OP_DIV,
  OP_MOD,
  OP_EQUAL,
  OP_LESS,
  OP_GREATER,
  OP_LNOT,
  OP_BNOT,
  OP_XOR,
  OP_OR,
  OP_AND,
  OP_SHL,
  OP_SHR,
  OP_MOVE,
  OP_RSP,
  OP_PUSH,
  OP_POP,
  OP_JUMP,
  OP_CJUMP,
  OP_RPC,
  OP_SPAWN,
  OP_HALT,
  OP_ACCEPT,
  OP_READ,
  OP_WRITE
};
enum address_mode {
  AM_IMMEDIATE,
  AM_MEMORY,
  AM_STACK,
  AM_STACK_MEMORY,
};

#define ARG(n, mode) (mode << (8 + n * 2))
#define LABEL(x) x
#define BLEN 138
static value BOOTLOADER[BLEN] = {
    OP_PUSH | ARG(0, AM_IMMEDIATE), 5,                       // push i5
    OP_MOVE | ARG(0, AM_IMMEDIATE) | ARG(1, AM_STACK), 0, 0, // move i0 s0
    OP_ACCEPT | ARG(0, AM_STACK), 1,                         // accept s1
    OP_READ | ARG(0, AM_STACK) | ARG(1, AM_STACK) | ARG(2, AM_MEMORY), 1, 2,
    1024, // read s1 s2 m1024
    OP_ADD | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 2,
    1024, 3,                                                    // a
    OP_MOVE | ARG(0, AM_STACK_MEMORY) | ARG(1, AM_STACK), 3, 3, // a
    OP_MUL | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 1,
    1024, 4, // a
    OP_ADD | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 4,
    8192, 4,                                                    // a
    OP_MOVE | ARG(0, AM_STACK) | ARG(1, AM_STACK_MEMORY), 1, 4, // a
    OP_SPAWN | ARG(0, AM_STACK) | ARG(1, AM_STACK), 3, 4,       // a
    OP_ADD | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 0, 1,
    0,                                                            // a
    OP_JUMP | ARG(0, AM_IMMEDIATE), 5,                            // jump i5
    OP_PUSH | ARG(0, AM_IMMEDIATE), 5,                            // push i5
    OP_MOVE | ARG(0, AM_IMMEDIATE) | ARG(0, AM_STACK), 0xb000, 0, // move i0 s0
    OP_MOVE | ARG(0, AM_STACK) | ARG(1, AM_STACK), 0, 1,          // move s0 s1
    OP_MOVE | ARG(0, AM_IMMEDIATE) | ARG(1, AM_STACK), 8, 2,      // move i0 s2
    // loopstart
    OP_READ | ARG(0, AM_STACK) | ARG(1, AM_STACK) | ARG(2, AM_STACK), 5, 3,
    4,                                                   // read s5 s3 s4
    OP_LNOT | ARG(0, AM_STACK) | ARG(1, AM_STACK), 4, 4, // lnot s4 s4
    OP_CJUMP | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE), 4,
    LABEL(60), // cjump s4 i<after halt>
    OP_HALT,   // halt
    // after halt
    OP_EQUAL | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 3,
    33, 4, OP_CJUMP | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE), 4,
    LABEL(130), // cjump s4 i<exe>
    OP_LESS | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 3, 35,
    4, // less s3 i96 s4
    OP_CJUMP | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE), 4,
    LABEL(94), // cjump s4 i<setam>
    OP_LESS | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 3, 58,
    4, // less s3 i58 s4
    OP_CJUMP | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE), 4,
    LABEL(112), // cjump s4 i<wdigit>
    OP_SUB | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK_MEMORY),
    3, 65, 1, // sub s3 i65 a1
    OP_ADD | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK_MEMORY),
    1, 1, 1,                                                 // add s1 i1 s1
    OP_MOVE | ARG(0, AM_IMMEDIATE) | ARG(1, AM_STACK), 0, 2, // move i0 s2
    OP_JUMP | ARG(0, AM_IMMEDIATE), LABEL(49), // jump i<loopstart>
    // setam
    OP_SUB | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 3, 35,
    3, // sub s3 i35 s3
    OP_SHR | ARG(0, AM_STACK) | ARG(1, AM_STACK) | ARG(2, AM_STACK), 3, 2,
    3, // shr s3 s2 s3
    OP_OR | ARG(0, AM_STACK_MEMORY) | ARG(1, AM_STACK) |
        ARG(2, AM_STACK_MEMORY),
    1, 3, 1, // or a1 s3 a1
    OP_ADD | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 2, 2,
    2,                                         // add s2 i2 s2
    OP_JUMP | ARG(0, AM_IMMEDIATE), LABEL(49), // jump i<loopstart>
    // wdigit
    OP_SUB | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 3, 48,
    4, // sub s3 i48 s4
    OP_MUL | ARG(0, AM_IMMEDIATE) | ARG(1, AM_STACK_MEMORY) |
        ARG(2, AM_STACK_MEMORY),
    10, 1, 1, // mul i10 a1 a1
    OP_ADD | ARG(0, AM_STACK) | ARG(1, AM_STACK_MEMORY) |
        ARG(2, AM_STACK_MEMORY),
    4, 1, 1, // add s4 a1 a1
    OP_ADD | ARG(0, AM_STACK) | ARG(1, AM_IMMEDIATE) | ARG(2, AM_STACK), 1, 1,
    1,                                         // add s1 i1 s1
    OP_JUMP | ARG(0, AM_IMMEDIATE), LABEL(49), // jump i<loopstart>,
    // exe
    OP_MOVE | ARG(0, AM_STACK) | ARG(1, AM_STACK), 5, 3, // move s5 s3
    OP_MOVE | ARG(0, AM_IMMEDIATE) | ARG(1, AM_STACK), LABEL(49),
    4,                             // move i<loopstart> s4
    OP_JUMP | ARG(0, AM_STACK), 0, // jump s0
};

value *memory;
value memory_size;
int sockfd;
int threads = 1;

value load(value address) {
  if (address >= 0 && address < memory_size)
    return memory[address];
  else {
    printf("WARN: out of bounds load from %li\n", address);
    return ~0;
  }
}
void store(value address, value val) {
  // printf("store addr=%li val=%li\n", address, val);
  if (address >= 0 && address < memory_size)
    memory[address] = val;
}

value load(value address);
void store(value address, value val);

value input_arg(value inst, value *pc, value sp, int n) {
  enum address_mode mode = (inst >> (8 + n * 2)) & 0b11;
  value arg = load(*pc);
  // printf("input pc=%li sp=%li, n=%i, inst=%li, mode=%i\n", *pc, sp, n, inst,
  //        mode);
  *pc += 1;
  switch (mode) {
  case AM_IMMEDIATE:
    return arg;
  case AM_MEMORY:
    return load(arg);
  case AM_STACK:
    return load(arg + sp);
  case AM_STACK_MEMORY:
    return load(load(arg + sp));
  }
}
void output_arg(value inst, value *pc, value sp, int n, value res) {
  enum address_mode mode = (inst >> (8 + n * 2)) & 0b11;
  value arg = load(*pc);
  // printf("input pc=%li sp=%li, n=%i, inst=%li, mode=%i\n", *pc, sp, n, inst,
  //        mode);
  *pc += 1;
  switch (mode) {
  case AM_IMMEDIATE:
    return;
  case AM_MEMORY:
    return store(arg, res);
  case AM_STACK:
    return store(arg + sp, res);
  case AM_STACK_MEMORY:
    return store(load(arg + sp), res);
  }
}
#define ARG_IN(n) input_arg(inst, &pc, sp, n)
#define ARG_OUT(n, x) output_arg(inst, &pc, sp, n, x)

void core(value pc, value sp) {
  char buf;
  while (1) {
    value inst = load(pc++);
    enum opcode op = inst & 0xff;
    value x, y, z;
    // printf("op=%i\n", op);
    switch (op) {
    case OP_NOA:
      printf("NOA\n");
      break;
    case OP_ADD:
      printf("ADD\n");
      ARG_OUT(2, ARG_IN(0) + ARG_IN(1));
      break;
    case OP_SUB:
      printf("SUB\n");
      ARG_OUT(2, ARG_IN(0) - ARG_IN(1));
      break;
    case OP_MUL:
      printf("MUL\n");
      ARG_OUT(2, ARG_IN(0) * ARG_IN(1));
      break;
    case OP_DIV:
      printf("DIV\n");
      ARG_OUT(2, ARG_IN(0) / ARG_IN(1));
      break;
    case OP_MOD:
      printf("MOD\n");
      ARG_OUT(2, ARG_IN(0) % ARG_IN(1));
      break;
    case OP_EQUAL:
      printf("EQUAL\n");
      ARG_OUT(2, ARG_IN(0) == ARG_IN(1));
      break;
    case OP_LESS:
      printf("LESS\n");
      ARG_OUT(2, ARG_IN(0) < ARG_IN(1));
      break;
    case OP_GREATER:
      printf("GREATER\n");
      ARG_OUT(2, ARG_IN(0) > ARG_IN(1));
      break;
    case OP_LNOT:
      printf("LNOT\n");
      ARG_OUT(1, !ARG_IN(0));
      break;
    case OP_BNOT:
      printf("BNOT\n");
      ARG_OUT(1, ~ARG_IN(0));
      break;
    case OP_XOR:
      printf("XOR\n");
      ARG_OUT(2, ARG_IN(0) ^ ARG_IN(1));
      break;
    case OP_OR:
      printf("OR\n");
      ARG_OUT(2, ARG_IN(0) | ARG_IN(1));
      break;
    case OP_AND:
      printf("AND\n");
      ARG_OUT(2, ARG_IN(0) & ARG_IN(1));
      break;
    case OP_SHL:
      printf("SHL\n");
      ARG_OUT(2, ARG_IN(0) << ARG_IN(1));
      break;
    case OP_SHR:
      printf("SHR\n");
      ARG_OUT(2, ARG_IN(0) >> ARG_IN(1));
      break;
    case OP_MOVE:
      printf("MOVE\n");
      ARG_OUT(1, ARG_IN(0));
      break;
    case OP_RSP:
      printf("RSP\n");
      ARG_OUT(0, sp);
      break;
    case OP_PUSH:
      printf("PUSH\n");
      sp -= ARG_IN(0);
      break;
    case OP_POP:
      printf("POP\n");
      sp += ARG_IN(0);
      break;
    case OP_JUMP:
      printf("JUMP\n");
      pc = ARG_IN(0);
      break;
    case OP_CJUMP:
      printf("CJUMP\n");
      if (ARG_IN(0))
        pc = ARG_IN(1);
      else
        ARG_IN(1);
      break;
    case OP_RPC:
      printf("RPC\n");
      ARG_OUT(0, pc);
      break;
    case OP_SPAWN:
      printf("SPAWN\n");
      if (threads < 16) {
        threads++;
      } else {
        fprintf(stderr, "too many threads\n");
        continue;
      }
      x = ARG_IN(0), y = ARG_IN(1);
      z = fork();
      if (z < 0) {
        perror("fork failed");
        return;
      } else if (z == 0) {
        pc = x;
        sp = y;
      }
      break;
    case OP_HALT:
      printf("HALT\n");
      return;
    case OP_ACCEPT:
      printf("ACCEPT\n");
      x = accept(sockfd, NULL, NULL);
      if (x < 0) {
        perror("accept failed");
        return;
      }
      ARG_OUT(0, x); // TODO dont pass fd
      break;
    case OP_READ:
      printf("READ\n");
      x = ARG_IN(0);
      z = recv(x, &buf, 1, 0);
      if (z < 0) {
        perror("read failed");
        close(x);
        return;
      }
      z = z == 0;
      y = buf;
      ARG_OUT(1, y);
      ARG_OUT(2, z);
      break;
    case OP_WRITE:
      printf("WRITE\n");
      x = ARG_IN(0);
      buf = ARG_IN(1);
      z = send(x, &buf, 1, 0);
      if (z < 0) {
        perror("write failed");
        close(x);
        z = 1;
      } else {
        z = 0;
      }
      ARG_OUT(2, z);
      break;
    default:
      fprintf(stderr, "unknown instruction %i\n", op);
      return;
    }
  }
}

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("usage: processor <port>\n");
    return EXIT_FAILURE;
  }
  struct sockaddr_in addr = {0};
  addr.sin_family = AF_INET;
  addr.sin_port = htons(atoi(argv[1]));
  addr.sin_addr.s_addr = inet_addr("0.0.0.0");
  sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (sockfd < 0) {
    perror("socket failed");
    close(sockfd);
    return EXIT_FAILURE;
  }

  int res = bind(sockfd, (struct sockaddr *)&addr, sizeof(addr));
  if (res < 0) {
    perror("bind failed");
    close(sockfd);
    return EXIT_FAILURE;
  }
  res = listen(sockfd, 1);
  if (res < 0) {
    perror("listen failed");
    close(sockfd);
    return EXIT_FAILURE;
  }

  memory_size = 1 << 24;
  memory = malloc(memory_size * sizeof(value));
  if (!memory) {
    fprintf(stderr, "cannot allocate enough memory\n");
    close(sockfd);
    return EXIT_FAILURE;
  }

  for (int i = 0; i < memory_size; i++) {
    memory[i] = -1;
  }
  for (int i = 0; i < BLEN; i++) {
    memory[i] = BOOTLOADER[i];
  }
  for (int i = 0; i < 256; i++) {
    memory[1024 + i] = 38;
  }

  core(0, memory_size - 1);
  close(sockfd);
  return EXIT_SUCCESS;
}
